#! /bin/bash

set -ex

which vagrant

cd $(dirname $0)
test -d roles || mkdir roles
if [ ! -e roles/ansible_role_install_buildbot ]; then
    ln -s ../.. roles/ansible_role_install_buildbot
fi

cd roles-test-vm
vagrant destroy -f
vagrant up
